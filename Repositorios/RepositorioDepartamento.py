from Repositorios.InterfacesRepositorio import InterfaceRepositorio
from Modelos.Departamento import Departamento


class RepositorioDepartamento(InterfaceRepositorio[Departamento]):
    pass
